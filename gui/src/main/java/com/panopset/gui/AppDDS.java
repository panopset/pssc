package com.panopset.gui;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public interface AppDDS {
  Image getFaviconImage();
  Pane createPane(FxDoc fxDoc);
  String getDescription();
  String getTitle();
  int getDefaultWidth();
  int getDefaultHeight();
  void open();
  void save();
  void saveAs();
  void exit();
  public void doLinksAfterShow(Stage stage, Scene scene);
}
