package com.panopset.gui;

import java.util.Properties;
import javafx.stage.Stage;

public class FxDoc {

  private final Stage stage;
  private final Properties properties = new Properties();
  
  public FxDoc(Stage stage) {
    this.stage = stage;
  }
  
  public Stage getStage() {
    return stage;
  }

  public void close() {
    stage.close();
  }
}
