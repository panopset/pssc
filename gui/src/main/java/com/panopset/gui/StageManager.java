package com.panopset.gui;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class StageManager {

  public void assembleAndShow(AppDDS dds, FxDoc fxDoc, Image faviconImage) {
    assembleAndShow(dds, fxDoc);
  }

  public void assembleAndShow(AppDDS dds, FxDoc fxDoc) {
    Stage stage = fxDoc.getStage();
    if (dds.getFaviconImage() != null) {
      stage.getIcons().add(dds.getFaviconImage());
    }
    stage.setTitle(dds.getTitle());
    stage.setWidth(dds.getDefaultWidth());
    stage.setHeight(dds.getDefaultHeight());
    Scene scene = new Scene(dds.createPane(new FxDoc(stage)), dds.getDefaultWidth(), dds.getDefaultHeight());
    stage.setScene(scene);
    stage.centerOnScreen();
    stage.show();
    dds.doLinksAfterShow(stage, scene);
  }
}
