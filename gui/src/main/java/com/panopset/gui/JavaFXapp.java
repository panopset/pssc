package com.panopset.gui;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.stage.Stage;

public class JavaFXapp extends Application implements Runnable {

  private static JavaFXapp INSTANCE;
  
  private static List<FxDoc> FXDOCS = new ArrayList<FxDoc>();

  @Override
  public void start(Stage stage) {
    addAndShow(stage);
    INSTANCE = this;
  }

  @Override
  public void run() {
    launch();
  }

  static AppDDS dds;

  private void addStage() {
    addAndShow(new Stage());
  }

  private void addAndShow(Stage stage) {
    FxDoc fxDoc = new FxDoc(stage);
    FXDOCS.add(fxDoc);
    new StageManager().assembleAndShow(dds, fxDoc);
  }
  
  public static void exit() {
    for (FxDoc fxDoc : FXDOCS) {
      fxDoc.close();
    }
  }

  public static void newWindow() {
    INSTANCE.addStage();
  }
}
