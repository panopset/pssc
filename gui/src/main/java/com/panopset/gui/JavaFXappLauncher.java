package com.panopset.gui;

import javafx.scene.layout.Pane;

public abstract class JavaFXappLauncher implements AppDDS {
  
  public abstract Pane createPane(FxDoc fxDoc);
  
  public void go() {
    try {
      new Thread(getApp()).start();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public JavaFXappLauncher() {
    this(1000, 900);
  }

  public JavaFXappLauncher(int width, int height) {
    this.defaultWidth = width;
    this.defaultHeight = height;
  }

  @Override
  public int getDefaultWidth() {
    return defaultWidth;
  }

  @Override
  public int getDefaultHeight() {
    return defaultHeight;
  }

  private JavaFXapp app;

  private JavaFXapp getApp() {
    if (app == null) {
      if (JavaFXapp.dds != null) {
        throw new RuntimeException("One JavaFX application per runtime allowed.");
      }
      JavaFXapp.dds = this;
      app = new JavaFXapp();
    }
    return app;
  }

  private final int defaultWidth, defaultHeight;
}
