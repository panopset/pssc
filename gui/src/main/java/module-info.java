module com.panopset.gui {
  requires transitive javafx.base;
  requires transitive javafx.graphics;
  requires transitive javafx.fxml;
  requires transitive javafx.controls;
  requires transitive com.panopset.util;
  exports com.panopset.gui;
}
