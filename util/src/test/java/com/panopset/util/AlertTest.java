package com.panopset.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class AlertTest implements CharPan {

  @Test
  void testAllAlerts() {
    Alert.debug(FOO);
    Alert.green(FOO);
    Alert.yellow(FOO);
    Alert.red(FOO);
    Alert.turnOnDebugging();
    Alert.debug(FOO);
    assertEquals(FOO, FOO);
  }

}
