package com.panopset.util;

import static org.junit.jupiter.api.Assertions.*;
import java.util.List;
import org.junit.jupiter.api.Test;

public class StringopTest implements CharPan {

  @Test
  public void testSimpleRegex() {
    assertTrue(Stringop.isRegexFound("foo", "foo"));
    assertTrue(Stringop.isRegexFound("foo", "foobar"));
    assertTrue(Stringop.isRegexFound("foo", "xfoobar"));
    assertFalse(Stringop.isRegexFound("foo", "bar"));
  }
  @Test
  public void testStringop() {
    assertTrue(("\r\n").equals(DOS_RTN));
  }

  @Test
  public void test() {
    assertTrue(Stringop.isPopulated("x"));
    assertFalse(Stringop.isPopulated("\t "));
    assertFalse(Stringop.isPopulated("\t"));
    assertEquals("Foo", Stringop.capitalize("foo"));
    assertEquals("A", Stringop.capitalize("a"));
    assertEquals(Stringop.DOS_RTN, Stringop.getEol());
    Stringop.setEol("\n");
    assertEquals("\n", Stringop.getEol());
    assertEquals(Stringop.countReturns("\n\nasdf\n"), 3);
    assertEquals(Stringop.countReturns(""), 0);
  }

  @Test
  public void testPullAfter() {
    assertEquals(
        Stringop.pullAfter("p_swing_apps/src/main/java/com/panopset/jnlp/games", "src/main/java/"),
        "com/panopset/jnlp/games");
  }

  @Test
  public void testTrimTrailingEmptyLines() {
    String[] foo = new String[] {"foo", "", "bar", ""};
    assertArrayEquals(new String[] {"foo", "", "bar"}, Stringop.trimTrailingEmptyLines(foo));
  }

  @Test
  public void testConcats() {
    assertEquals("foobar", Stringop.concat("foo", "bar"));
    assertEquals("", Stringop.concat("", ""));
    assertEquals("foobar", Stringop.concat("foo", "", "bar"));
    assertEquals("foo", Stringop.concat(null, "foo"));
    List<String> disambiguationOfMethods = null;
    assertEquals("", Stringop.concat(disambiguationOfMethods));
  }
}
