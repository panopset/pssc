package com.panopset.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import com.panopset.util.Alert;

public class FileHexDumper {
  private final File file;

  public FileHexDumper(File file) {
    this.file = file;
  }

  private File getFile() {
    return file;
  }

  public String dump(int start, Integer max, int width, boolean isSpaces,
      boolean isCharsetIncluded) {
    if (getFile() == null) {
      return Alert.red("No file was selected.", getFile());
    }
    if (!getFile().exists()) {
      return Alert.red("File does not exist.", getFile());
    }
    try (FileInputStream fis = new FileInputStream(getFile())) {
      return Hexop.dump(fis, start, max, width, isSpaces, isCharsetIncluded, file.length());
    } catch (IOException e) {
      Alert.red(e);
      return e.getMessage();
    }
  }

  public String dump(int bytes) {
    return dump(0, bytes, bytes, false, false);
  }

}
