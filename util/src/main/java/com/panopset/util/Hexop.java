package com.panopset.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import org.apache.commons.io.input.ReaderInputStream;

public class Hexop implements CharPan {
  private Hexop() {}

  public static String dump(InputStream inpStrm, int start, Integer max, int width, boolean isSpaces,
      boolean isCharsetIncluded, long streamLength) {

    StringWriter rtnWriter = new StringWriter();
    StringWriter hexWriter = new StringWriter();
    StringWriter repWriter = new StringWriter();

    try (BufferedInputStream bis =
        inpStrm instanceof BufferedInputStream ? (BufferedInputStream) inpStrm
            : new BufferedInputStream(inpStrm);) {

      int i = 0;
      int lineByteCount = 0;
      if (start > 0) {
        if ((long) start > streamLength) {
          rtnWriter.append(Joinop.on(" ").join(new Object[] {"File length of", streamLength,
              "is smaller than the start position."}));
        }

        bis.skip((long) start);
      }

      int c = bis.read();

      while (true) {
        do {
          if (!hasMore(i++, max, c)) {
            return rtnWriter.toString();
          }

          if (lineByteCount > 0 && isSpaces) {
            hexWriter.append(" ");
            repWriter.append(" ");
          }

          String hexRep = String.format("%02X", c);
          hexWriter.append(hexRep);
          String backSlasher = backSlashOf(c, hexRep);
          if (backSlasher.length() > 0) {
            repWriter.append(backSlasher);
          } else {
            repWriter.append(" ");
            Character ch = (char) c;
            if (Character.isWhitespace(ch)) {
              repWriter.append(" ");
            } else {
              repWriter.append(Character.toString((char) c));
            }
          }

          c = bis.read();
          ++lineByteCount;
        } while (lineByteCount < width && c != -1);

        if (isCharsetIncluded) {
          rtnWriter.append(repWriter.toString());
          if (width > 0) {
            rtnWriter.append("\n");
          }
        }

        rtnWriter.append(hexWriter.toString());
        if (width > 0) {
          rtnWriter.append("\n");
        }

        hexWriter = new StringWriter();
        repWriter = new StringWriter();
        lineByteCount = 0;
      }
    } catch (Exception ex) {
      Alert.red(ex);
      return rtnWriter.toString();
    }
  }

  private static String backSlashOf(int c, String hexRep) {
    if (!isInBackSlashRange(c)) {
      return "";
    } else if (hexRep == null) {
      return "";
    } else if ("09".equals(hexRep)) {
      return "\\t";
    } else if ("08".equals(hexRep)) {
      return "\\b";
    } else if ("0A".equals(hexRep)) {
      return "\\n";
    } else if ("0D".equals(hexRep)) {
      return "\\r";
    } else {
      return "0C".equals(hexRep) ? "\\f" : "";
    }
  }

  private static boolean isInBackSlashRange(int c) {
    return c < 14;
  }

  private static boolean hasMore(int i, int max, int c) {
    if (c != -1 && max > 0 && i >= max) {
      return true;
    } else {
      return c != -1;
    }
  }

  public static String dump(File file, int bytes) {
    return dump(file, 0, bytes, bytes, false, false);
  }

  public static String dump(File file, int start, Integer max, int width, boolean isSpaces,
      boolean isCharsetIncluded) {
    if (file == null) {
      return "No file was selected.";
    }
    if (!file.exists()) {
      return "File " +  Fileop.getCanonicalPath(file) +  " does not exist.";
    }
    try (FileInputStream fis = new FileInputStream(file)) {
      return dump(fis, start, max, width, isSpaces, isCharsetIncluded, file.length());
    } catch (IOException e) {
      Alert.red(e);
      return e.getMessage();
    }
  }
  
  public static String dump(String string) {
    return dump(new StringReader(string), 0, string.length(), 0, false, false,
        string.length());
  }

  public static String dump(StringReader stringReader, int start, int max, int width, boolean isSpaces,
      boolean isCharsetIncluded, int length) {
    return dump(new ReaderInputStream(stringReader, UTF_8), start, max, width, isSpaces,
        isCharsetIncluded, (long) length);
  }
}
