package com.panopset.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Stringop implements CharPan {
  private static String eol;
  
  public static boolean isRegexFound(String regex, String value) {
    return new RegexTester(regex).matches(value);
  }

  public static String check4match(final String s1, final String s2, final String r1,
      final String r2) {
    if (s1 == null || s2 == null || r1 == null || r2 == null) {
      return "";
    }
    if (s1.equals(s2)) {
      return r1;
    }
    return r2;
  }

  public static String capitalize(final String str) {
    if (str != null && str.length() > 0) {
      if (str.length() > 1) {
        return ("" + str.charAt(0)).toUpperCase() + str.substring(1);
      }
      return str.toUpperCase();
    }
    return "";
  }

  public static boolean isPopulated(final String value) {
    return value != null && value.trim().length() > 0;
  }

  public static String[] toArray(final List<String> list) {
    String[] rtn = new String[list.size()];
    int idx = 0;
    for (String s : list) {
      rtn[idx++] = s;
    }
    return rtn;
  }

  public static String pullAfter(String str, String searchStr) {
    if (str == null || searchStr == null) {
      return "";
    }
    if (str.length() < searchStr.length()) {
      return "";
    }
    int i = str.indexOf(searchStr);
    if (i == -1) {
      return "";
    }
    return str.substring(i + searchStr.length());
  }

  public static String getEol() {
    if (eol == null) {
      eol = DOS_RTN;
    }
    return eol;
  }

  public static void setEol(final String value) {
    eol = value;
  }

  public static String wrapFixedWidth(final String str, final int width) {
    if (!isPopulated(str)) {
      return "";
    }
    if (width < 1) {
      return str;
    }
    StringWriter sw = new StringWriter();
    for (int i = 0; i < str.length(); i++) {
      sw.append(str.charAt(i));
      if ((i + 1) % width == 0) {
        sw.append(getEol());
      }
    }
    return sw.toString();
  }

  public static int countReturns(final String str) {
    if (str == null) {
      return 0;
    }
    int count = 0;
    for (int i = 0; i < str.length(); i++) {
      char ch = str.charAt(i);
      if (ch == '\n') {
        count++;
      }
    }
    return count;
  }

  /**
   * All trailing empty Strings are removed.
   *
   * @param sa Array of new Stringz().
   * @return String array that ends with the last populated a entry.
   */
  public static String[] trimTrailingEmptyLines(final String[] sa) {
    if (sa == null) {
      return new String[] {};
    }
    int jdx = sa.length;
    while (jdx > 0 && !isPopulated(sa[jdx - 1])) {
      jdx = jdx - 1;
    }
    if (jdx == sa.length) {
      return sa;
    }
    List<String> rslt = new ArrayList<String>();
    for (int i = 0; i < jdx; i++) {
      rslt.add(sa[i]);
    }
    return toArray(rslt);
  }

  /**
   * Where is a varargs concatenator when you need one. Write my own until I find one in commons or
   * guava or something. Empty String is returned if args is null. Otherwise all non-null element in
   * arg is concatenated in order.
   *
   * @param args Strings to join.
   * @return Concatenated args in one String.
   */
  public static String concat(final String... args) {
    return concat(Arrays.asList(args));
  }

  /**
   * Concatenate a String List.
   *
   * @param pts Parts to concatenate.
   * @return Concatenated parts.
   */
  public static String concat(final List<String> pts) {
    if (pts == null) {
      return "";
    }
    StringWriter sw = new StringWriter();
    for (String s : pts) {
      if (s != null) {
        sw.append(s);
      }
    }
    return sw.toString();
  }

  public static List<String> toList(final String[] lines) {
    List<String> rtn = new ArrayList<String>();
    for (String s : lines) {
      rtn.add(s);
    }
    return rtn;
  }

  // JAVA 8.
  public static List<String> linesToList(final String inp) {
    List<String> rtn = new ArrayList<String>();
    StringReader sr = new StringReader(inp);
    try (BufferedReader br = new BufferedReader(sr)) {
      String s = br.readLine();
      while (s != null) {
        rtn.add(s);
        s = br.readLine();
      }
    } catch (IOException ex) {
      Alert.red(ex);
    }
    return rtn;
  }

  public static List<String> getLines(final String str) {
    List<String> rtn = new ArrayList<String>();

    try (StringReader sr = new StringReader(str);

        BufferedReader br = new BufferedReader(sr);) {
      String line = br.readLine();
      while (line != null) {
        rtn.add(line);
        line = br.readLine();
      }
    } catch (IOException e) {
      Alert.red(e);
    }
    return rtn;
  }

  public Stringop() {}

  public static String join(String glue, Object... args) {
    StringWriter sw = new StringWriter();
    boolean firstTime = true;

    for (Object obj : args) {
      if (firstTime) {
        firstTime = false;
      } else {
        sw.append(glue);
      }

      sw.append(obj.toString());
    }

    return sw.toString();
  }

  public static String getDollarString(long pennies) {
    return NumberFormat.getCurrencyInstance().format((new BigDecimal(pennies))
        .setScale(2, RoundingMode.HALF_UP).divide(new BigDecimal(100), RoundingMode.HALF_UP));
  }

  public static int parse(String value) {
    if (!isPopulated(value)) {
      return 0;
    } else {
      String str = value.replace(",", "");

      try {
        return Integer.parseInt(str);
      } catch (NumberFormatException var3) {
        Alert.red(value);
        Alert.red(var3);
        throw var3;
      }
    }
  }

  public static int parse(String value, Integer base) {
    if (!isPopulated(value)) {
      return -1;
    } else {
      String str = value.replace(",", "");

      try {
        return Integer.parseInt(str, base);
      } catch (NumberFormatException var4) {
        Alert.red(var4);
        return -1;
      }
    }
  }
}
