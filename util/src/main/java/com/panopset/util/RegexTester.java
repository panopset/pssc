package com.panopset.util;

import java.util.regex.Pattern;

public class RegexTester {

  Pattern p;

  public RegexTester(String regex) {
    if (Stringop.isPopulated(regex)) {
      p = Pattern.compile(regex);
    }
  }

  public boolean matches(String value) {
    if (p == null) {
      return false;
    }
    return p.matcher(value).find();
  }

}
