package com.panopset.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import com.panopset.Nls;

public class Fileop implements CharPan {
  public Fileop() {}

  public static File getUserHome() {
    return new File(System.getProperty("user.home"));
  }


  public static String getStandardPath(File file) {
    return getCanonicalPath(file).replace("\\", "/");
  }

  public static String getParentDirectory(final File file) {
    if (file.exists()) {
      File fullFile = new File(getCanonicalPath(file));
      return getCanonicalPath(fullFile.getParentFile());
    }
    return getCanonicalPath(file.getParentFile());
  }

  public static boolean fileCanRead(File file) {
    if (file == null) {
      return false;
    } else if (file.getPath().isEmpty()) {
      return false;
    } else if (!fileExists(file)) {
      return false;
    } else if (file.canRead()) {
      return true;
    } else {
      Alert.yellow("Can't read " + getCanonicalPath(file));
      return false;
    }
  }
  
  public static boolean fileExists(File file) {
    if (file == null) {
      Alert.yellow("No file selected.");
      return false;
    } else {
      return file.exists();
    }
  }

  public static File createTempFile(String fileName) {
    return new File(Joinop.on(FILE_SEP).join(new Object[] {TEMP_DIR_PATH, fileName}));
  }

  public static void write(String[] strs, File file) {
    try (FileWriter fw = new FileWriter(file);

        BufferedWriter bw = new BufferedWriter(fw)) {
      for (String s : strs) {
        bw.write(s);
        bw.write(EOL);
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public static void write(String str, File file) {
    try (FileWriter fw = new FileWriter(file);

        BufferedWriter bw = new BufferedWriter(fw);) {
      bw.write(str);
    } catch (IOException ex) {
      ex.printStackTrace();
    }

  }

  public static BufferedReader getReader(File file) throws IOException {
    FileReader fr = new FileReader(file);
    return new BufferedReader(fr);
  }

  public static BufferedWriter getWriter(File file) throws IOException {
    FileWriter fw = new FileWriter(file);
    return new BufferedWriter(fw);
  }

  public static boolean canRead(File file) {
    return file != null && file.exists() && file.canRead();
  }

  public static String getCanonicalPath(File file) {
    if (file == null) {
      return "File is null.";
    }
    try {
      return file.getCanonicalPath();
    } catch (IOException ex) {
      Alert.red(ex);
      return ex.getMessage();
    }
  }

  public static boolean isFileOneOfExtensions(File file, String extsToTry) {
    if (extsToTry != null && file != null) {
      String fileExtension = getExtension(file);
      if (extsToTry.contains(",")) {
        String[] exts = extsToTry.split(",");
        for (String ext : exts) {
          if (ext.trim().equalsIgnoreCase(fileExtension)) {
            return true;
          }
        }
        return false;
      } else {
        return getExtension(file).equalsIgnoreCase(extsToTry);
      }
    } else {
      return false;
    }
  }

  public static String getExtension(File file) {
    if (file == null) {
      return "";
    } else {
      String fileName = file.getName();
      int i = fileName.lastIndexOf(46);
      return i > 0 ? fileName.substring(i + 1) : "";
    }
  }

  public static String combinePaths(String dirPath, String path) {
    return combinePaths(new File(dirPath), path);
  }

  public static String combinePaths(File dir, String path) {
    return Joinop.on(FILE_SEP).join(new Object[] {getCanonicalPath(dir), path});
  }

  public static String loadSmallText(String filePath) {
    StringBuilder text = new StringBuilder();
    File file = new File(filePath);
    try (BufferedReader br = new BufferedReader(new FileReader(file))) {
      String line;
      while ((line = br.readLine()) != null) {
        text.append(line);
        text.append(Stringop.getEol());
      }
    } catch (IOException ex) {
      Alert.red(ex);
    }
    return text.toString();
  }
  public static Map<String, String> loadMapFromProperties(final Properties properties) {
    final Map<String, String> rtn = new HashMap<String, String>();
    for (Object key : properties.keySet()) {
      rtn.put(key.toString(), properties.getProperty(key.toString()));
    }
    return rtn;
  }

  public static Map<String, String> loadPropsFromFile(final File file) {
    Properties props = new Properties();
    loadProperties(props, file);
    return loadMapFromProperties(props);
  }
  
  public static void loadProperties(final Properties properties, final File file) {
    if (!Fileop.isAvailable(file)) {
      return;
    }
    try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr)) {
      properties.load(br);
      br.close();
      fr.close();
    } catch (IOException e) {
      Alert.red(e);
    }
  }
  
  public static void delete(final File file) {
    if (file == null) {
      return;
    }
    if (!file.exists()) {
      return;
    }
    if (!file.delete()) {
      Alert.yellow(file, Nls.xlate("Failed to delete"));
    }
  }

  public static boolean isAvailable(final File file) {
    if (file == null) {
      Alert.red("null file.");
      return false;
    }
    if (file.exists()) {
      return true;
    }
    File parent = file.getParentFile();
    if (parent == null) {
      return false;
    }
    parent.mkdirs();
    try {
      return file.createNewFile();
    } catch (IOException e) {
      Alert.red(e);
      return false;
    }
  }

  public static boolean fileCanWrite(File file) {
    if (file == null) {
      Alert.yellow("file");
      return false;
    }
    String path = getCanonicalPath(file);
    File parentDir = file.getParentFile();
    if (parentDir == null) {
      return false;
    }
    parentDir.mkdirs();
    if (!file.exists()) {
      try {
        file.createNewFile();
      } catch (IOException e) {
        Alert.yellow(
            Nls.xlate("Can't create") +  " " + path);
      }
    }
    if (!file.canWrite()) {
      Alert.yellow(Nls.xlate("Can't write") + " " +  path);
      return false;
    }
    return true;
  }
  
  public static void createParentDirectories(final File file) {
    if (!file.exists()) {
      File parentDir = file.getParentFile();
      if (parentDir == null) {
        // Check to see if the file has a relative path.
        File f2 = new File(getCanonicalPath(file));
        parentDir = f2.getParentFile();
      }
      if (parentDir.exists()) {
        return;
      }
      mkdirs(parentDir);
    }
  }

  public static void mkdirs(final File path) {
    if (path == null) {
      return;
    }
    if (path.exists()) {
      return;
    }
    if (!path.mkdirs()) {
      Alert.red(Nls.xlate("Unable to create path to") + " " + getCanonicalPath(path));
    }
  }

  
  public static int countLines(final File file) {
    try (InputStream is = new BufferedInputStream(new FileInputStream(file))) {
      byte[] c = new byte[1024];
      int count = 0;
      int readChars = 0;
      boolean empty = true;
      while ((readChars = is.read(c)) != -1) {
        empty = false;
        for (int i = 0; i < readChars; ++i) {
          if (c[i] == '\n') {
            ++count;
          }
        }
      }
      return (count == 0 && !empty) ? 1 : count;
    } catch (IOException ex) {
      Alert.red(ex);
      return -1;
    }
  }

 public static boolean verifyCanWrite(File file) {
    if (file == null) {
      Alert.red("Failed to write due to null file.");
      return false;
    }
    if (!file.getParentFile().exists() && !file.getParentFile().mkdirs()) {
      Alert.red("Can not write to:" + Fileop.getCanonicalPath(file));
      Alert.red(Fileop.getCanonicalPath(file.getParentFile()));
    }
    return false;
  }

  public static boolean verifyCanWrite(String path) {
    if (path == null) {
      Alert.red("Failed to write due to null file path.");
      return false;
    }
    return verifyCanWrite(new File(path));
  }

  public static final File TEMP_DIRECTORY = new File(USER_HOME + "/temp");
  public static final String CRLF = "\r\n";
  public static final String FILE_SEP = System.getProperty("file.separator");
  public static final String PATH_SEP = System.getProperty("path.separator");
}
