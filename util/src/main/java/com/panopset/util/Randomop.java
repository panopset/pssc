package com.panopset.util;

import java.util.concurrent.ThreadLocalRandom;

public enum Randomop {
  INSTANCE;

  private static final ThreadLocalRandom RANDOM = ThreadLocalRandom.current();

  private Randomop() {
  }

  public static int random(int min, int max) {
    if (min == max) {
      return min;
    } else {
      int shift = min;
      if (min > max) {
        shift = max;
      }
      return RANDOM.nextInt(Math.abs(max - min) + 1) + shift;
    }
  }

  public static int nextInt() {
    return RANDOM.nextInt();
  }

  public static long nextLong() {
    return RANDOM.nextLong();
  }
}
