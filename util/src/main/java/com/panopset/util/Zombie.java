package com.panopset.util;

import java.util.ArrayList;
import java.util.List;

public enum Zombie {

  INSTANCE;

  /**
   * Will be true for the life of the application. All threads should check here
   * to see if they should terminate.
   */
  private boolean active = true;

  /**
   * Stop application. There is no restart, kills all threads.
   */
  public static void stop() {
    INSTANCE.active = false;
    INSTANCE.stopEverything();
  }

  private void stopEverything() {
    for (Runnable sa : stopActions) {
      sa.run();
    }
  }

  /**
   * @return true if active.
   */
  public static boolean isActive() {
    return INSTANCE.active;
  }

  public static void addStopAction(Runnable runnable) {
    INSTANCE.add(runnable);
  }

  private void add(Runnable runnable) {
    stopActions.add(runnable);
  }
  
  private List<Runnable> stopActions = new ArrayList<Runnable>();
}
