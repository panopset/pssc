package com.panopset.util;

import java.nio.charset.StandardCharsets;

public interface CharPan {
  String UTF_8 = StandardCharsets.UTF_8.name();
  String TAB = "\t";
  String JAVA_RTN = "\n";
  String COPYRIGHT = "©";
  String LINE_SEPARATOR = System.getProperty("line.separator");
  int LINE_FEED_VALUE = 10;
  String LINE_FEED = "\n";
  int CARRIAGE_RETURN_VALUE = 13;
  String CARRIAGE_RETURN = "\r";
  String DOS_RTN = "\r\n";
  String USER_HOME = System.getProperty("user.home");
  String FILE_SEP = System.getProperty("file.separator");
  String PATH_SEP = System.getProperty("path.separator");
  String USER_DIR = System.getProperty("user.dir");
  String TEMP_DIR_PATH = USER_HOME + FILE_SEP + "tmp";
  String EOL = System.getProperty("line.separator");
  String LAST_MODIFIED_FORMAT = "EEEE, MMMM dd, yyyy, h:mm a(zz)";
  String FOO = "foo";
  String BAR = "bar";
  String BAT = "bat";
}
