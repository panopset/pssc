package com.panopset.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum Alert implements CharPan {
  INSTANCE;

  private Alert() {
  }

  public static void red(Throwable tr) {
    INSTANCE.error(tr);
  }

  public static void stop(Throwable tr) {
    red(tr);
    throw new RuntimeException(tr);
  }

  public static String yellow(Throwable tr) {
    INSTANCE.warn(tr);
    return tr.getMessage();
  }

  public static String yellow(String message, Throwable tr) {
    yellow(message);
    yellow(tr);
    return message;
  }

  public static String yellow(String message) {
    INSTANCE.warn(message);
    return message;
  }

  public static void green(String message) {
    INSTANCE.info(message);
  }

  public static void green(String message, File file) {
    green(Joinop.on(":").join(new Object[]{message, Fileop.getCanonicalPath(file)}));
  }

  public static String red(String message) {
    return INSTANCE.error(message);
  }

  public static String red(String message, File file) {
    return red(Joinop.on(":").join(new Object[]{message, Fileop.getCanonicalPath(file)}));
  }

  public static void red(IOException ex, File file) {
    red((Throwable)ex);
    red(ex.getMessage(), file);
  }

  public static String yellow(File file, String message) {
    String path = Fileop.getCanonicalPath(file);
    return yellow(Joinop.on(":").join(new Object[]{path, message}));
  }

  public static void debug(String message) {
    INSTANCE.dbg(message);
  }

  public static String getStackTrace(Throwable throwable) {
    StringWriter sw = new StringWriter();

    try {
      PrintWriter pw = new PrintWriter(sw);
      throwable.printStackTrace(pw);
      pw.flush();
      pw.close();
      sw.close();
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return sw.toString();
  }

  public static String getStackTraceAndCauses(Throwable throwable) {
    StringWriter sw = new StringWriter();
    sw.append("See log");
    sw.append(": ");
    sw.append(throwable.getMessage());
    sw.append(EOL);
    sw.append("*************************");
    sw.append(EOL);
    sw.append(getStackTrace(throwable));
    sw.append(EOL);

    for(Throwable cause = throwable.getCause(); cause != null; cause = cause.getCause()) {
      sw.append("*************************");
      sw.append(EOL);
      sw.append(getStackTrace(cause));
      sw.append(EOL);
    }

    return sw.toString();
  }

  private String error(String msg) {
    Logger.getGlobal().log(Level.SEVERE, msg);
    return msg;
  }

  private void error(Throwable tr) {
    Logger.getGlobal().log(Level.SEVERE, tr.getMessage(), tr);
    throw new RuntimeException(tr);
  }

  private void info(String msg) {
    Logger.getGlobal().log(Level.INFO, msg);
  }

  private void warn(String msg) {
    Logger.getGlobal().log(Level.WARNING, msg);
  }

  private void warn(Throwable tr) {
    Logger.getGlobal().log(Level.WARNING, tr.getMessage(), tr);
  }

  private void dbg(String message) {
    Logger.getGlobal().log(Level.FINEST, message);
  }

  public static void debug(String[] sa) {
    if (sa != null) {
      String[] var1 = sa;
      int var2 = sa.length;
      for(int var3 = 0; var3 < var2; ++var3) {
        String msg = var1[var3];
        debug(msg);
      }

    }
  }

  public static void addAppender(Handler handler) {
    INSTANCE.addHandler(handler);
  }

  private void addHandler(Handler handler) {
    Logger.getGlobal().addHandler(handler);
  }

  public static void turnOnDebugging() {
    Logger rootLog = Logger.getLogger("");
    rootLog.setLevel( Level.ALL );
    rootLog.getHandlers()[0].setLevel( Level.ALL ); 
  }
}
