package com.panopset.util;

public class Joinop {
  private final String glue;

  public Joinop(String glue) {
    this.glue = glue;
  }

  public static Joinop on(String glue) {
    return new Joinop(glue);
  }

  public String join(Object... strs) {
    return Stringop.join(this.glue, strs);
  }
}
