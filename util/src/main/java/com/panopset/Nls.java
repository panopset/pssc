package com.panopset;

/**
 * Calling the x method of this class, prepares it for its inclusion in a CASE
 * generated NLS bundle file.
 *
 * @author Karl Dinwiddie
 *
 */
public final class Nls {

  /**
   * @param value
   *          English String to be included in bundle later.
   * @return value.
   */
  public static String xlate(final String value) {
    return value;
  }

  /**
   * Prevent instantiation.
   */
  private Nls() {
  }
}
