package com.panopset.lowerclass;

import java.io.File;
import com.panopset.util.CharPan;
import com.panopset.util.StatusListener;

public class VersionParser implements StatusListener, CharPan {

  public static String DEFAULT_MAVEN_HOME = USER_HOME + "/.m2";
  public static String PANOPSET_JAR = USER_HOME + "/panopset.jar";

  public static void main(String... args) {
    System.out.println("*** Entire repository example:");
    System.out.println(new VersionParser().getReport());
    System.out.println("*** Single jar example:");
    System.out.println(new VersionParser(PANOPSET_JAR).getReport());
  }

  public VersionParser() {}

  public VersionParser(final String pathToJar_or_directoryToTraverse) {
    this(new File(pathToJar_or_directoryToTraverse));
  }

  public VersionParser(final File jar_or_directoryToTraverse) {
    setFile(jar_or_directoryToTraverse);
  }

  private String mavenHome;

  private String getMavenHome() {
    if (mavenHome == null) {
      mavenHome = System.getenv().get("M2_HOME");
      if (mavenHome == null) {
        mavenHome = System.getenv().get("MAVEN_HOME");
      }
      if (mavenHome == null) {
        mavenHome = DEFAULT_MAVEN_HOME;
      }
    }
    return mavenHome;
  }

  private File file;

  private File getFile() {
    if (file == null) {
      file = new File(getMavenHome());
    }
    return file;
  }

  public void setFile(final File jarOrDirectory) {
    this.file = jarOrDirectory;
  }
  
  public String getReport() {
    VersionMakeup vm = new VersionMakeup();
    vm.analyze(getFile());
    return vm.getReport();
  }

  public void update(String message) {
    System.out.println(message);
  }
}
