package com.panopset.lowerclass;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import org.apache.commons.io.FilenameUtils;
import com.panopset.util.Alert;
import com.panopset.util.Fileop;
import com.panopset.util.Joinop;
import com.panopset.util.Stringop;

class VersionMakeup {

  private Map<String, Map<MajorVersion, Integer>> map =
      Collections.synchronizedSortedMap(new TreeMap<>());

  private void clear() {
    map.clear();
    classReport = new StringWriter();
  }

  public void analyze(File file) {
    clear();
    genRpt(file.getName(), file);
  }

  public String getReport() {
    StringWriter tp = new StringWriter();
    for (Entry<String, Map<MajorVersion, Integer>> e : map.entrySet()) {
      tp.append(e.getKey());
      tp.append(" > ");
      Map<MajorVersion, Integer> jvs = e.getValue();
      boolean firstTime = true;
      for (Entry<MajorVersion, Integer> jv : jvs.entrySet()) {
        if (firstTime) {
          firstTime = false;
        } else {
          tp.append(",");
        }
        tp.append(jv.getKey().strRep);
        tp.append(" " + jv.getValue());
      }
      tp.append("\n");
    }
    if (Stringop.isPopulated(classReport.toString())) {
      tp.append("\n");
      tp.append("\n");
      tp.append(classReport.toString());
    }
    return tp.toString();
  }

  private void genRpt(String fileName, File file) {
    if (file == null) {
      Alert.yellow("No file selected.");
      return;
    }
    if (!file.exists()) {
      Alert.yellow(file, "File doesn't exist.");
      return;
    }
    if (file.isDirectory()) {
      genDirectoryReport(fileName, file);
    } else {
      String ext = FilenameUtils.getExtension(file.getName());
      if ("class".equals(ext)) {
        genClassReport(fileName, file, true);
      } else if ("jar".equals(ext)) {
        genJarReport(fileName, file, false);
      }
    }
    Alert.green(Joinop.on(":").join("genReport complete for ", Fileop.getCanonicalPath(file)));
  }

  private void genDirectoryReport(String fileName, File file) {
    File[] list = file.listFiles();
    if (list != null) {
      for (File f : list) {
        if (f.isDirectory()) {
          genDirectoryReport(fileName, f);
        } else {
          String ext = FilenameUtils.getExtension(f.getName());
          if ("class".equals(ext)) {
            genClassReport(fileName, f, false);
          } else if ("jar".equals(ext)) {
            genJarReport(fileName, f, false);
          }
        }
      }
    }
  }

  private void genClassReport(String fileName, File file, boolean printDetails) {
    updateReportMap(fileName, updateStatsForClass(fileName, file, printDetails));
  }

  private void genJarReport(String fileName, File file, boolean printDetails) {
    Alert.green(Joinop.on(":").join("Processing jar ", file.getName()));
    updateReportMap(file.getName(), updateStatsForJar(file, printDetails));
  }

  private void updateReportMap(String name, Map<MajorVersion, Integer> jvs) {
    if (jvs != null && !jvs.isEmpty()) {
      map.put(name, jvs);
    }
  }

  private Map<MajorVersion, Integer> updateStatsForJar(File dirFile, boolean printDetails) {
    Map<MajorVersion, Integer> jvs = createVersionMap();
    try (JarFile jar = new JarFile(dirFile)) {
      Enumeration<JarEntry> enumEntries = jar.entries();
      while (enumEntries.hasMoreElements()) {
        java.util.jar.JarEntry entry = (java.util.jar.JarEntry) enumEntries.nextElement();
        java.io.InputStream is = jar.getInputStream(entry);
        try (DataInputStream dis = new DataInputStream(is)) {
          ClassVersion cv = readClassVersion(entry.getName(), dis, printDetails);
          if (cv != null && cv.isValid()) {
            Integer count = jvs.get(cv.getMajorVersion());
            if (count == null) {
              jvs.put(cv.getMajorVersion(), 1);
            } else {
              jvs.put(cv.getMajorVersion(), count + 1);
            }
          }
        } catch (IOException ex) {
          throw new RuntimeException(ex);
        }
      }
    } catch (IOException ex) {
      Alert.red(Fileop.getCanonicalPath(dirFile));
      throw new RuntimeException(ex);
    }
    return jvs;
  }

  private StringWriter classReport = new StringWriter();

  private Map<MajorVersion, Integer> updateStatsForClass(String name, File dirFile,
      boolean printDetails) {
    Alert.green(Joinop.on(":").join("Processing class ", name));
    Map<MajorVersion, Integer> jvs = createVersionMap();
    try (FileInputStream fis = new FileInputStream(dirFile);
        DataInputStream dis = new DataInputStream(fis)) {
      ClassVersion cv = readClassVersion(name, dis, printDetails);
      if (cv != null && cv.isValid()) {
        jvs.put(cv.getMajorVersion(), 1);
      }
    } catch (IOException ex) {
      Alert.red(ex);
    }
    return jvs;
  }

  private ClassVersion readClassVersion(String name, DataInputStream dis, boolean printDetails)
      throws IOException {
    if (dis.available() > 0) {
      if ("class".equals(FilenameUtils.getExtension(name))) {
        int magic = dis.readInt();
        if (magic != 0xcafebabe) {
          Alert.red(name + " is not a java class! this should be 0xcafebabe:"
              + Integer.toHexString(magic));
        } else {
          int minor = dis.readUnsignedShort();
          int major = dis.readUnsignedShort();
          ClassVersion classVersion =
              new ClassVersion(MajorVersion.findFromHexString(Integer.toHexString(major)),
                  Integer.toHexString(minor));
          if (printDetails) {
            classReport.append(name);
            classReport.append(" major: ");
            classReport.append(classVersion.getMajorVersion().strRep);
            classReport.append(" minor: ");
            classReport.append(classVersion.getMinorVersion());
            classReport.append("\n");
          }
          return classVersion;
        }
      }
    }
    return null;
  }

  private Map<MajorVersion, Integer> createVersionMap() {
    return Collections.synchronizedSortedMap(new TreeMap<>());
  }
}
