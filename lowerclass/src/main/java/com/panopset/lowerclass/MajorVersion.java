package com.panopset.lowerclass;

import com.panopset.util.Joinop;
import com.panopset.util.Stringop;

public enum MajorVersion {

  JDK1_1(0x2D, "JDK 1.1"),

  JDK1_2(0x2E, "JDK 1.2"),

  JDK1_3(0x2F, "JDK 1.3"),

  JDK1_4(0x30, "JDK 1.4"),

  JSE5_0(0x31, "Java SE 5"),

  JSE6_0(0x32, "Java SE 6"),

  JSE7(0x33, "Java SE 7"),

  JSE8(0x34, "Java SE 8"),

  JSE9(0x35, "Java SE 9"),

  JSE10(0x35, "Java SE 10");

  MajorVersion(int major, String strRep) {
    this.major = major;
    this.strRep = strRep;
  }

  private final int major;
  
  public final String strRep;

  public static MajorVersion findFromHexString(String hexRep) {
    if (hexRep == null) {
      throw new NullPointerException();
    }
    if (hexRep.length() != 2) {
      throw new RuntimeException(Joinop.on(":").join("Invalid hex rep", hexRep));
    }
    int val = Stringop.parse(hexRep, 16);
    for (MajorVersion jv : MajorVersion.values()) {
      if (val == jv.major) {
        return jv;
      }
    }
    throw new RuntimeException("Undefined hex rep " + hexRep);
  }
}
