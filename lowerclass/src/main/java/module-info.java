module com.panopset.lowerclass {
  requires transitive com.panopset.util;
  requires org.apache.commons.io;
  exports com.panopset.lowerclass;
}
