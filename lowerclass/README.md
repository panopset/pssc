# Panopset lowerclass project

Open source Java repository and jar analysis tool
that quickly provides a report on the Java major release levels used to create the jars.

When the report is run on a single jar, a full report on each class is also available.

## Use Cases

### Entire repository

    System.out.println(new VersionParser().getReport());
    
This will print out a list of all jars in the default repository (${user.home}/.m2),
along with a list of JDK major version release names, followed by the number of classes
in the jar that were compiled at that release level.

The format is:

    (jar simple name) > (release) (number of classes)...

The report can show, for example, that if your target environment is Java 7, the most recent xstream release you can use is 1.4.7:

    xstream-1.4.2.jar > Java SE 5 401
    xstream-1.4.3.jar > Java SE 5 400
    xstream-1.4.7.jar > Java SE 5 437
    xstream-1.4.9.jar > Java SE 5 444,Java SE 8 2

To specify a path to start the recursive traversal of a directory, pass the path in the constructor:
    
    System.out.println(new VersionParser("/home/user/.m2").getReport());

### Single jar

Pass the path to a Jar (java.io.File objects are also accepted) to the constructor, to get a full report on a jar.  Especially useful for composite single, or fat jars.

    System.out.println(new VersionParser("/home/user/panopset.jar").getReport());

## GUI

If [panopset.jar](https://panopset.com/download.html) is avalable in your path (placed in your home directory, for example) and you have the latest Java installed, this will also work:

    java -cp panopset.jar com.panopset.jnlp.apps.LowerClass
    
![gui](https://panopset.com/images/lowerclass.png "gui")

# Requirements

* Latest Java.
* Latest Maven.

# Source

    git clone https://gitlab.com/panopset/pssc.git
